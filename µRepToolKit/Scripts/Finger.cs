using System.Numerics;
using System.Diagnostics;
using System.Globalization;
using Microsoft.MixedReality.Toolkit;
using Microsoft.MixedReality.Toolkit.Input;
using Microsoft.MixedReality.Toolkit.UI;
using Microsoft.MixedReality.Toolkit.Utilities;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Reflection;
using System.Threading;
using TMPro;
using UnityEngine;
using Microgestures;


namespace Microgestures 
{
    public class Thumb : TwoZonesActor
    {
        public override ActorEnum[] getActorTypes() 
            { return new ActorEnum[1]{
                ActorEnum.Thumb};
            }

        override protected Tuple<TrackedHandJoint, float>[] getTip() 
            { return new Tuple<TrackedHandJoint, float>[1]{
                Tuple.Create(TrackedHandJoint.ThumbTip, 1f)}; }
        override protected Tuple<TrackedHandJoint, float>[] getProximal() 
            { return new Tuple<TrackedHandJoint, float>[1]{
                Tuple.Create(TrackedHandJoint.ThumbProximalJoint, 1f)}; }
        public Thumb(Handedness handedness) : base(handedness){}
    }
    
    public class Index : ThreeZonesActor
    {
        public override ActorEnum[] getActorTypes() 
            { return new ActorEnum[1]{
                ActorEnum.Index};
            }
        override protected Tuple<TrackedHandJoint, float>[] getTip() 
            { return new Tuple<TrackedHandJoint, float>[1]{
                Tuple.Create(TrackedHandJoint.IndexTip, 1f)}; }
        override protected Tuple<TrackedHandJoint, float>[] getCenter() 
            { return new Tuple<TrackedHandJoint, float>[1]{
                Tuple.Create(TrackedHandJoint.IndexMiddleJoint, 1f)}; }
        override protected Tuple<TrackedHandJoint, float>[] getBasis() 
            { return new Tuple<TrackedHandJoint, float>[1]{
                Tuple.Create(TrackedHandJoint.IndexKnuckle, 1f)}; }
        public Index(Handedness handedness) : base(handedness){}
    }
    
    public class Middle : ThreeZonesActor
    {
        public override ActorEnum[] getActorTypes() 
            { return new ActorEnum[1]{
                ActorEnum.Middle};
            }
        override protected Tuple<TrackedHandJoint, float>[] getTip() 
            { return new Tuple<TrackedHandJoint, float>[1]{
                Tuple.Create(TrackedHandJoint.MiddleTip, 1f)}; }
        override protected Tuple<TrackedHandJoint, float>[] getCenter() 
            { return new Tuple<TrackedHandJoint, float>[1]{
                Tuple.Create(TrackedHandJoint.MiddleMiddleJoint, 1f)}; }
        override protected Tuple<TrackedHandJoint, float>[] getBasis() 
            { return new Tuple<TrackedHandJoint, float>[1]{
                Tuple.Create(TrackedHandJoint.MiddleKnuckle, 1f)}; }
        public Middle(Handedness handedness) : base(handedness){}
    }
    
    public class Ring : ThreeZonesActor
    {
        public override ActorEnum[] getActorTypes() 
            { return new ActorEnum[1]{
                ActorEnum.Ring};
            }
        override protected Tuple<TrackedHandJoint, float>[] getTip() 
            { return new Tuple<TrackedHandJoint, float>[1]{
                Tuple.Create(TrackedHandJoint.RingTip, 1f)}; }
        override protected Tuple<TrackedHandJoint, float>[] getCenter() 
            { return new Tuple<TrackedHandJoint, float>[1]{
                Tuple.Create(TrackedHandJoint.RingMiddleJoint, 1f)}; }
        override protected Tuple<TrackedHandJoint, float>[] getBasis() 
            { return new Tuple<TrackedHandJoint, float>[1]{
                Tuple.Create(TrackedHandJoint.RingKnuckle, 1f)}; }
        public Ring(Handedness handedness) : base(handedness){}
    }
    
    public class Pinky : TwoZonesActor
    {
        public override ActorEnum[] getActorTypes() 
            { return new ActorEnum[1]{
                ActorEnum.Pinky};
            }
        override protected Tuple<TrackedHandJoint, float>[] getTip() 
            { return new Tuple<TrackedHandJoint, float>[1]{
                Tuple.Create(TrackedHandJoint.PinkyTip, 1f)}; }
        override protected Tuple<TrackedHandJoint, float>[] getProximal() 
            { return new Tuple<TrackedHandJoint, float>[1]{
                Tuple.Create(TrackedHandJoint.PinkyKnuckle, 1f)}; }
        public Pinky(Handedness handedness) : base(handedness){}
    }
}