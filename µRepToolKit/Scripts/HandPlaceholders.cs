using System.Numerics;
using System.Diagnostics;
using System.Globalization;
using Microsoft.MixedReality.Toolkit;
using Microsoft.MixedReality.Toolkit.Input;
using Microsoft.MixedReality.Toolkit.UI;
using Microsoft.MixedReality.Toolkit.Utilities;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Reflection;
using System.Threading;
using TMPro;
using UnityEngine;
// using UnityEditor;
// using UnityEditor.UIElements;
// using UnityEngine.UIElements;


[AddComponentMenu("HandPlaceholders", 0)]
public class HandPlaceholders : MonoBehaviour
{
    public Hand Lhand;
    public Hand Rhand;

    [System.Serializable]
    public class Hand
    {
        public Digit thumb;
        public Finger index;
        public Finger middle;
        public Finger ring;
        public Finger pinky;

        protected Handedness handedness;

        [System.Serializable]
        public class Digit
        {
            public FingerObject tip;
            public FingerObject center;
            protected List<FingerObject> fingers;
            protected MixedRealityPose pose;
            public virtual void instantiate(Transform transform, string typeOfFinger)
            {
                fingers = new List<FingerObject>();
                if (tip.obj) {
                    tip.instantiate(transform, Tuple.Create(getTip(typeOfFinger), 1f));
                    fingers.Add(tip);
                }
                if (center.obj)
                {
                    center.instantiate(transform, Tuple.Create(getTip(typeOfFinger), 0.33f), Tuple.Create(getMiddle(typeOfFinger), 0.67f));
                    fingers.Add(center);
                }
            }
            
            public void update(bool state, Handedness handedness) {
                foreach (FingerObject fingerObject in fingers) {
                    if (state && fingerObject.visibleJoints(handedness, out pose))
                    {
                        fingerObject.obj.transform.position = pose.Position;
                        fingerObject.obj.transform.rotation = pose.Rotation;
                        if (fingerObject.obj) {
                            fingerObject.obj.SetActive(true);
                        }
                    } else {
                        fingerObject.obj.SetActive(false);
                    }
                }
            }
            protected TrackedHandJoint getTip(string typeOfFinger) {
                switch(typeOfFinger) {
                    case "Thumb":
                        return TrackedHandJoint.ThumbTip;
                    case "Index":
                        return TrackedHandJoint.IndexTip;
                    case "Middle":
                        return TrackedHandJoint.MiddleTip;
                    case "Ring":
                        return TrackedHandJoint.RingTip;
                    case "Pinky":
                        return TrackedHandJoint.PinkyTip;
                    default:
                        return TrackedHandJoint.None;
                }
            }
            protected TrackedHandJoint getMiddle(string typeOfFinger) {
                switch(typeOfFinger) {
                    case "Thumb":
                        return TrackedHandJoint.ThumbProximalJoint;
                    case "Index":
                        return TrackedHandJoint.IndexMiddleJoint;
                    case "Middle":
                        return TrackedHandJoint.MiddleMiddleJoint;
                    case "Ring":
                        return TrackedHandJoint.RingMiddleJoint;
                    case "Pinky":
                        return TrackedHandJoint.PinkyMiddleJoint;
                    default:
                        return TrackedHandJoint.None;
                }
            }
            protected TrackedHandJoint getKnuckle(string typeOfFinger) {
                switch(typeOfFinger) {
                    case "Thumb":
                        return TrackedHandJoint.None;
                    case "Index":
                        return TrackedHandJoint.IndexKnuckle;
                    case "Middle":
                        return TrackedHandJoint.MiddleKnuckle;
                    case "Ring":
                        return TrackedHandJoint.RingKnuckle;
                    case "Pinky":
                        return TrackedHandJoint.PinkyKnuckle;
                    default:
                        return TrackedHandJoint.None;
                }
            }
        }

        [System.Serializable]
        public class Finger : Digit
        {
            public FingerObject basis;

            public override void instantiate(Transform transform, string typeOfFinger)
            {
                fingers = new List<FingerObject>();
                if (tip.obj) {
                    tip.instantiate(transform, Tuple.Create(getTip(typeOfFinger), 1f));
                    fingers.Add(tip);
                }
                if (center.obj)
                {
                    center.instantiate(transform, Tuple.Create(getTip(typeOfFinger), 0.33f), Tuple.Create(getMiddle(typeOfFinger), 0.67f));
                    fingers.Add(center);
                }
                if (basis.obj)
                {
                    basis.instantiate(transform, Tuple.Create(getMiddle(typeOfFinger), 0.67f), Tuple.Create(getKnuckle(typeOfFinger), 0.33f));
                    fingers.Add(basis);
                }
            }
        }

        [System.Serializable]
        public struct FingerObject
        {
            public GameObject obj;
            Tuple<TrackedHandJoint, float>[] joints;

            public void instantiate(Transform transform, params Tuple<TrackedHandJoint, float>[] joints) 
            {    
                this.obj = Instantiate(this.obj, transform);
                this.joints = joints;
            }
            MixedRealityPose localpose;

            public bool visibleJoints(Handedness handedness, out MixedRealityPose pose) {
                UnityEngine.Vector3 pos = new UnityEngine.Vector3();
                UnityEngine.Quaternion rot = new UnityEngine.Quaternion();

                foreach (Tuple<TrackedHandJoint, float> joint in joints) {
                    if (HandJointUtils.TryGetJointPose(joint.Item1, handedness, out localpose)) {
                        pos += localpose.Position*joint.Item2;
                        rot = localpose.Rotation;
                    } else {
                        pose = new MixedRealityPose();
                        return false;
                    }
                }
                pose = new MixedRealityPose(pos, rot);
                return true;
            }
        }

        List<FingerObject> fingers = new List<FingerObject>();


        public void instantiate(Handedness handedness, Transform transform)
        {
            this.handedness = handedness;

            thumb.instantiate(transform, "Thumb");
            index.instantiate(transform, "Index");
            middle.instantiate(transform, "Middle");
            ring.instantiate(transform, "Ring");
            pinky.instantiate(transform, "Pinky");
        }

        public void update(bool state)
        {
            thumb.update(state, this.handedness);
            index.update(state, this.handedness);
            middle.update(state, this.handedness);
            ring.update(state, this.handedness);
            pinky.update(state, this.handedness);
        }
    }

    void Start()
    {
        Lhand.instantiate(Handedness.Left, this.transform);
        Rhand.instantiate(Handedness.Right, this.transform);
    }

    void Update()
    {
        Lhand.update(state);
        Rhand.update(state);
    }
    private bool state;

    public void setActive() {
        state = true;
    }

    public void setInactive() {
        state = false;
    }
}