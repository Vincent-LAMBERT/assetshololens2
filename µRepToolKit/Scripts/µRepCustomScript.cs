using Microsoft.MixedReality.Toolkit.Input;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Microgestures;

namespace Microgestures
{
    [AddComponentMenu("µRepCustomScript", 0)]
    public class µRepCustomScript: MonoBehaviour
    {
        public void Start() {
            // For testing purposes
        }
    }
}
