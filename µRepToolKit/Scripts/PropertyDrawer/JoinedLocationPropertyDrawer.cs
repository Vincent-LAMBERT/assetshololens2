using System.Numerics;
using System.Diagnostics;
using System.Globalization;
using Microsoft.MixedReality.Toolkit;
using Microsoft.MixedReality.Toolkit.Input;
using Microsoft.MixedReality.Toolkit.UI;
using Microsoft.MixedReality.Toolkit.Utilities;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Reflection;
using System.Threading;
using System.ComponentModel;
using TMPro;
using UnityEngine;
using Microgestures;
using UnityEngine.UIElements;


#if UNITY_EDITOR
using UnityEditor;
using UnityEditor.UIElements;
[CustomPropertyDrawer (typeof(JoinedLocation))]
public class JoinedLocationPropertyDrawer : LocationPropertyDrawer
{    
    JoinedActorEnum all_actor;

    MainJoinedFingerEnum mainActor = MainJoinedFingerEnum.Index;
    IndexJoinedFingerEnum indexMate = IndexJoinedFingerEnum.Middle;
    MiddleJoinedFingerEnum middleMate = MiddleJoinedFingerEnum.Index;
    RingJoinedFingerEnum ringMate = RingJoinedFingerEnum.Middle;
    PinkyJoinedFingerEnum pinkyMate = PinkyJoinedFingerEnum.Ring;

    protected override void initializeProperties(SerializedProperty property) {
        actorProp = property.FindPropertyRelative ("actor");

        all_actor = Location.getJoinedActorEnum((ActorEnum) actorProp.enumValueIndex);

        switch (all_actor) {
            case JoinedActorEnum.IndexJoinedMiddle:
                mainActor = MainJoinedFingerEnum.Index;
                indexMate = IndexJoinedFingerEnum.Middle;
                break;
            case JoinedActorEnum.MiddleJoinedIndex:
                mainActor = MainJoinedFingerEnum.Middle;
                middleMate = MiddleJoinedFingerEnum.Index;
                break;
            case JoinedActorEnum.MiddleJoinedRing:
                mainActor = MainJoinedFingerEnum.Middle;
                middleMate = MiddleJoinedFingerEnum.Ring;
                break;
            case JoinedActorEnum.RingJoinedMiddle:
                mainActor = MainJoinedFingerEnum.Ring;
                ringMate = RingJoinedFingerEnum.Middle;
                break;
            case JoinedActorEnum.RingJoinedPinky:
                mainActor = MainJoinedFingerEnum.Ring;
                ringMate = RingJoinedFingerEnum.Pinky;
                break;
            case JoinedActorEnum.PinkyJoinedRing:
                mainActor = MainJoinedFingerEnum.Pinky;
                pinkyMate = PinkyJoinedFingerEnum.Ring;
                break;
            default:
                throw new Exception("Error on JoinedLocationProperyDrawer");
        };
    }

    protected override void OnConditionnalGUI (SerializedProperty property) {
        if (property.isArray) {
            EditorGUI.PropertyField(tools.getCurrentPosition(), property, true);
        } else {
            initializeLocationPropertyHeight();
            tools.initialize();
            tools.beginHorizontal();
            mainActor = (MainJoinedFingerEnum) tools.insertEnum(mainActor, 0.5f);

            switch (mainActor) {
                case MainJoinedFingerEnum.Index:
                    indexMate = (IndexJoinedFingerEnum) tools.insertEnum(indexMate, 0.5f);
                    actorProp.enumValueIndex = (int) ActorEnum.IndexJoinedMiddle;
                    break;
                case MainJoinedFingerEnum.Middle:
                    middleMate = (MiddleJoinedFingerEnum) tools.insertEnum(middleMate, 0.5f);
                    switch (middleMate) {
                        case MiddleJoinedFingerEnum.Index:
                            actorProp.enumValueIndex = (int) ActorEnum.MiddleJoinedIndex;
                            break;
                        case MiddleJoinedFingerEnum.Ring:
                            actorProp.enumValueIndex = (int) ActorEnum.MiddleJoinedRing;
                            break;
                    }
                    break;
                case MainJoinedFingerEnum.Ring:
                    ringMate = (RingJoinedFingerEnum) tools.insertEnum(ringMate, 0.5f);
                    switch (ringMate) {
                        case RingJoinedFingerEnum.Middle:
                            actorProp.enumValueIndex = (int) ActorEnum.RingJoinedMiddle;
                            break;
                        case RingJoinedFingerEnum.Pinky:
                            actorProp.enumValueIndex = (int) ActorEnum.RingJoinedPinky;
                            break;
                    }
                    break;
                case MainJoinedFingerEnum.Pinky:
                    pinkyMate = (PinkyJoinedFingerEnum) tools.insertEnum(pinkyMate, 0.5f);
                    actorProp.enumValueIndex = (int) ActorEnum.PinkyJoinedRing;
                    break;
            }

            tools.endHorizontal();
        }
    }
}


#endif