using System.Numerics;
using System.Diagnostics;
using System.Globalization;
using Microsoft.MixedReality.Toolkit;
using Microsoft.MixedReality.Toolkit.Input;
using Microsoft.MixedReality.Toolkit.UI;
using Microsoft.MixedReality.Toolkit.Utilities;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Reflection;
using System.Threading;
using System.ComponentModel;
using TMPro;
using UnityEngine;
using Microgestures;
using UnityEngine.UIElements;


namespace Microgestures 
{
    [Serializable, AddComponentMenu("Placeholder", 0)]
    public class Placeholder
    {
        public FingersStatus fingersStatus;
        public UniqueLocation uniqueLocation;
        public JoinedLocation joinedLocation;
        public AwayLocation AwayLocation;

        public Placeholder()
        {
            this.uniqueLocation = new UniqueLocation(ActorEnum.Index, ThreeZoneActorZone.Tip);
        }

        public Placeholder(UniqueLocation location) { this.uniqueLocation = location; }
        public Placeholder(JoinedLocation location) { this.joinedLocation = location; }
        public Placeholder(AwayLocation location) { this.AwayLocation = location; }

        private FingersStatus getFingerStatus() { return fingersStatus; }
        private UniqueLocation getUniqueLocation() { return uniqueLocation; }
        private JoinedLocation getJoinedLocation() { return joinedLocation; }
        private AwayLocation getAwayLocation() { return AwayLocation; }

        public Location getLocation() { 
            switch (this.getFingerStatus()) {
                case FingersStatus.Unique:
                    return uniqueLocation;
                case FingersStatus.Joined:
                    return joinedLocation;
                default :
                    return AwayLocation;
            }
        }

        public Behavior getFingerStatusBehavior(Handedness handedness) {
            switch (this.getFingerStatus()) {
                case FingersStatus.Unique:
                    return Behavior.nothing(handedness);
                case FingersStatus.Joined:
                    return Behavior.transparencyOnDistance(handedness);
                default :
                    return Behavior.transparencyOnProximity(handedness);
            }
        }
    }

    public enum FingersStatus 
    {
        Unique,
        Joined,
        Away
    }
}