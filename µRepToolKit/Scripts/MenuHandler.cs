using Microsoft.MixedReality.Toolkit.Input;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[AddComponentMenu("MenuHandler", 0)]
public class MenuHandler: MonoBehaviour
{
    public HandPlaceholders[] scriptReferences;
    private int activeScript=0;

    public void Start() 
    {
        scriptReferences[0].setActive();
    }
    public void onTouch()
    {
        scriptReferences[activeScript].setInactive();
        activeScript++;
        if (activeScript >= scriptReferences.Length) {
            activeScript = 0;
        }
        scriptReferences[activeScript].setActive();
    }
}
