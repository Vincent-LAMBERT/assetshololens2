using System.Numerics;
using System.Diagnostics;
using System.Globalization;
using Microsoft.MixedReality.Toolkit;
using Microsoft.MixedReality.Toolkit.Input;
using Microsoft.MixedReality.Toolkit.UI;
using Microsoft.MixedReality.Toolkit.Utilities;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Reflection;
using System.Threading;
using TMPro;
using UnityEngine;
using Microgestures;


namespace Microgestures 
{
    public class IndexJoinedMiddle : OneZoneActor
    {
        public override ActorEnum[] getActorTypes() 
            { return new ActorEnum[2]{
                ActorEnum.IndexJoinedMiddle,
                ActorEnum.MiddleJoinedIndex};
            }
        public IndexJoinedMiddle(Handedness handedness) : base(handedness){}

        override protected Tuple<TrackedHandJoint, float>[] getTip() 
            { return new Tuple<TrackedHandJoint, float>[2]{
                Tuple.Create(TrackedHandJoint.IndexTip, 0.5f), 
                Tuple.Create(TrackedHandJoint.MiddleTip, 0.5f)}; }
    }

    public class MiddleJoinedRing : OneZoneActor
    {
        public override ActorEnum[] getActorTypes() 
            { return new ActorEnum[2]{
                ActorEnum.MiddleJoinedRing,
                ActorEnum.RingJoinedMiddle};
            }
        public MiddleJoinedRing(Handedness handedness) : base(handedness){}

        override protected Tuple<TrackedHandJoint, float>[] getTip() 
            { return new Tuple<TrackedHandJoint, float>[2]{
                Tuple.Create(TrackedHandJoint.MiddleTip, 0.5f), 
                Tuple.Create(TrackedHandJoint.RingTip, 0.5f)}; }
    }

    public class RingJoinedPinky : OneZoneActor
    {
        public override ActorEnum[] getActorTypes() 
            { return new ActorEnum[2]{
                ActorEnum.RingJoinedPinky,
                ActorEnum.PinkyJoinedRing};
            }
        public RingJoinedPinky(Handedness handedness) : base(handedness){}

        override protected Tuple<TrackedHandJoint, float>[] getTip() 
            { return new Tuple<TrackedHandJoint, float>[2]{
                Tuple.Create(TrackedHandJoint.RingMiddleJoint, 0.5f), 
                Tuple.Create(TrackedHandJoint.PinkyTip, 0.5f)}; }
    }
}