using System.Diagnostics;
using System.Globalization;
using Microsoft.MixedReality.Toolkit;
using Microsoft.MixedReality.Toolkit.Input;
using Microsoft.MixedReality.Toolkit.UI;
using Microsoft.MixedReality.Toolkit.Utilities;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Reflection;
using System.Threading;
using TMPro;
using UnityEngine;
using Microgestures;


namespace Microgestures 
{
    [System.Serializable]
    public struct ARObject
    {
        public GameObject obj;
        private Stack<Behavior> behaviors;
        Tuple<TrackedHandJoint, float>[] joints;
        MixedRealityPose localpose;
        List<Vector3> jointPositions;

        public ARObject(GameObject obj, Stack<Behavior> behaviors) {
            this.obj = obj;
            foreach (Behavior b in behaviors) {
                b.setInitialTransparency(obj);
            }
            this.behaviors = behaviors;
            this.joints = null;
            this.localpose = new MixedRealityPose();;
            this.jointPositions = null;
        }

        public void instantiate(Transform transform, params Tuple<TrackedHandJoint, float>[] joints) 
        {   
            this.obj = UnityEngine.Object.Instantiate(this.obj, transform);
            this.joints = joints;
        }

        public bool visibleJoints(Handedness handedness, bool wristOriented, out MixedRealityPose pose) {
            jointPositions = new List<Vector3> ();
            try {
                pose = getCenterJoint(handedness);
                if (wristOriented) {
                    // pose = getStartingJoint(handedness);
                    pose = getWristOrientedJoint(handedness);
                } else {
                    pose = getCenterJoint(handedness);
                }
            } catch (InvalidOperationException e) {
                pose = new MixedRealityPose();
                return false;
            }


            Vector3 headPosition = Camera.main.transform.position;
            Vector3 headForward = Camera.main.transform.forward;

            pose = tweakPoseToCorrectHololens(headPosition, pose);        

            useBehaviors(jointPositions);

            return true;
        }

        private MixedRealityPose tweakPoseToCorrectHololens(Vector3 OH, MixedRealityPose pose) {
            Vector3 OW = pose.Position;
            Vector3 HW = OW-OH;
            HW.Normalize();
            pose.Position -= HW*0.02f;
            return pose;
        }

        private MixedRealityPose getWristOrientedJoint(Handedness handedness) {
            Vector3 OA = getStartingJoint(handedness).Position;
            Vector3 OC = getCenterJoint(handedness).Position;
            Vector3 OW = getWristJoint(handedness).Position;

            Vector3 WA = OA-OW;
            Vector3 AC = OC-OA;

            return new MixedRealityPose(OA, Quaternion.LookRotation(AC, WA));
        }

        private MixedRealityPose getCenterJoint(Handedness handedness) {
            Vector3 pos = new Vector3();
            Quaternion rot = new Quaternion();
            foreach (Tuple<TrackedHandJoint, float> joint in joints) {
                if (HandJointUtils.TryGetJointPose(joint.Item1, handedness, out localpose)) {
                    pos += localpose.Position*joint.Item2;
                    rot = localpose.Rotation;
                    jointPositions.Add(localpose.Position);
                } else {
                    throw new InvalidOperationException("Hand not visible");
                }
            }
            return new MixedRealityPose(pos, rot);
        }

        private MixedRealityPose getStartingJoint(Handedness handedness) {
            if (HandJointUtils.TryGetJointPose(joints[0].Item1, handedness, out localpose)) {
                return new MixedRealityPose(localpose.Position, localpose.Rotation);
            } else {
                throw new InvalidOperationException("Hand not visible");
            }
        }

        private MixedRealityPose getWristJoint(Handedness handedness) {
            if (HandJointUtils.TryGetJointPose(TrackedHandJoint.Wrist, handedness, out localpose)) {
                return new MixedRealityPose(localpose.Position, localpose.Rotation);
            } else {
                throw new InvalidOperationException("Hand not visible");
            }
        }

        public void useBehaviors(List<Vector3> positions) {
            foreach (Behavior behavior in behaviors) {
                behavior.use(obj, positions);
            }
        }
    }
}